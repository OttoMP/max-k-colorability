#import math tools
import numpy as np

# We import the tools to handle general Graphs
import networkx as nx

# We import plotting tools
import pandas as pd
import matplotlib.pyplot as plt
from   matplotlib import cm
from   matplotlib.ticker import LinearLocator, FormatStrFormatter

# importing Qiskit
from qiskit.compiler import transpile
from qiskit.transpiler import PassManager
from qiskit import(
  QuantumCircuit,
  execute,
  Aer)
from qiskit.visualization import plot_histogram

# importing tools for running QAOA
import random
from scipy.optimize import minimize, fmin, Bounds

from qiskit import IBMQ
from qiskit.providers.ibmq      import least_busy
from qiskit.tools.monitor       import job_monitor


def show_figure(fig):
    new_fig = plt.figure()
    new_mngr = new_fig.canvas.manager
    new_mngr.canvas.figure = fig
    fig.set_canvas(new_mngr.canvas)
    plt.show(fig)

class QAOAMaxkColorability:

    def __init__(self, graph, num_colors):
        self.num_colors = num_colors
        self.graph = graph

        # Number of vertices in the graph
        self.num_nodes = len(graph)

        self.circuit = QuantumCircuit((self.num_nodes*self.num_colors),
                (self.num_nodes*self.num_colors))

    def initial_state(self, coloring):
        for i, color in enumerate(coloring):
            self.circuit.x((i*self.num_colors)+color)
        return

    def w3_state_generation(self):
        for i in range(self.num_nodes):
            self.circuit.ry(np.pi/4, i*self.num_colors+1)
            self.circuit.ry(np.arccos(-1/3), i*self.num_colors+2)
            self.circuit.rz(-np.pi/2, i*self.num_colors+2)
            self.circuit.cx(i*self.num_colors+2, i*self.num_colors+1)
            self.circuit.ry(-np.pi/4, i*self.num_colors+1)
            self.circuit.rz(np.pi/2, i*self.num_colors+1)
            self.circuit.cx(i*self.num_colors+2, i*self.num_colors+1)
            self.circuit.rz(np.pi/2, i*self.num_colors+1)
            self.circuit.cx(i*self.num_colors+1, i*self.num_colors)
            self.circuit.cx(i*self.num_colors+2, i*self.num_colors+1)
            self.circuit.rx(np.pi, i*self.num_colors+2)
        return

    def w2_state_generation(self):
        for i in range(self.num_nodes):
            self.circuit.h(i*self.num_colors)
            self.circuit.x(i*self.num_colors+1)
            self.circuit.cx(i*self.num_colors, i*self.num_colors+1)
        return

    def phase_separator(self, gamma):
        for edge in self.graph.edges():
            for k in range(self.num_colors):
                self.circuit.cx(int(edge[0])*self.num_colors+k,int(edge[1])*self.num_colors+k)
                self.circuit.rz(2*gamma, int(edge[1])*self.num_colors+k)
                self.circuit.cx(int(edge[0])*self.num_colors+k,int(edge[1])*self.num_colors+k)

    def partial_mixer(self, neighbour, target, beta):
        pass

    def mixer(self, beta):
        for node in self.graph.nodes():
            for j in range(self.num_colors-1):
                # Rxx
                self.circuit.h(int(node)*self.num_colors+j)
                self.circuit.h(int(node)*self.num_colors+j+1)
                self.circuit.cx(int(node)*self.num_colors+j,int(node)*self.num_colors+j+1)
                self.circuit.rz(2*beta, int(node)*self.num_colors+j+1)
                self.circuit.cx(int(node)*self.num_colors+j,int(node)*self.num_colors+j+1)
                self.circuit.h(int(node)*self.num_colors+j)
                self.circuit.h(int(node)*self.num_colors+j+1)

                # Ryy
                self.circuit.rx(-np.pi/2,int(node)*self.num_colors+j)
                self.circuit.rx(-np.pi/2,int(node)*self.num_colors+j+1)
                self.circuit.cx(int(node)*self.num_colors+j,int(node)*self.num_colors+j+1)
                self.circuit.rz(2*beta, int(node)*self.num_colors+j+1)
                self.circuit.cx(int(node)*self.num_colors+j,int(node)*self.num_colors+j+1)
                self.circuit.rx(np.pi/2,int(node)*self.num_colors+j)
                self.circuit.rx(np.pi/2,int(node)*self.num_colors+j+1)

    def measurement(self):
        qubits = [num for num in range(self.num_nodes*self.num_colors)]
        self.circuit.measure(qubits, qubits)
        return

# Compute the value of the cost function
def cost_function_C(x,G,num_colors):

    E = G.edges()

    C = 0
    for index in E:
        e1 = index[0]
        e2 = index[1]
        for color in range(num_colors):
            C += (x[int(e1)*num_colors + color]*x[int(e2)*num_colors + color])

    return C

def QAOA(par, p, G):
    # QAOA parameters
    middle = int(len(par)/2)
    gamma = par[:middle]
    beta = par[middle:]

    # Graph
    num_colors = 3

    QAOA_circ = QAOAMaxkColorability(G, num_colors)

    #QAOA_circ.w2_state_generation()
    QAOA_circ.w3_state_generation()
    for step in range(p):
        QAOA_circ.phase_separator(gamma[step])
        QAOA_circ.mixer(beta[step])
    QAOA_circ.measurement()

    # Execute the circuit on the selected backend
    backend = Aer.get_backend('qasm_simulator')
    backend_options = {"method" : "statevector_gpu"}
    shots = 1000

    job = execute(QAOA_circ.circuit, backend, shots=shots, backend_options=backend_options)

    # Grab results from the job
    result = job.result()

    # Evaluate the data from the simulator
    counts = result.get_counts()

    avr_C       = 0
    for sample in list(counts.keys()):
        # use sampled bit string x to compute C(x)
        x         = [int(num) for num in list(sample)]
        tmp_eng   = cost_function_C(x,G,num_colors)

        # compute the expectation value and energy distribution
        avr_C     = avr_C    + counts[sample]*tmp_eng

    Mp_sampled   = avr_C/shots

    return Mp_sampled

def save_csv(data, nome_csv):
    data_points = pd.DataFrame(data, columns=['Expected Value', 'p', 'Graph Number'])
    data_points.to_csv(nome_csv)

    return

def main():
    print("Starting program")

    print("Creating Graph list")
    G_list = []
    number = 0
    for _ in range(10):
        name = str(number)
        G_list.append(nx.read_adjlist("graphs/"+name+".adjlist"))
        number += 1

    G = G_list[0]

    for loop in range(4):
        p = 2**loop
        gamma = [random.uniform(0, 2*np.pi) for _ in range(p)]
        beta  = [random.uniform(0, np.pi) for _ in range(p)]

        print("Using following parameters:\n ")
        print("p: ", p)
        print("Gamma: ", gamma)
        print("Beta: ", beta)
        print("\n")

        # Graph
        num_colors = 3

        QAOA_circ = QAOAMaxkColorability(G, num_colors)

        #QAOA_circ.w2_state_generation()
        QAOA_circ.w3_state_generation()
        # Mixer 0
        for step in range(p):
            QAOA_circ.phase_separator(gamma[step])
            QAOA_circ.mixer(beta[step])
        QAOA_circ.measurement()

        # Execute the circuit on the selected backend
        backend = Aer.get_backend('qasm_simulator')
        shots = 1000
        optimized_3 = transpile(QAOA_circ.circuit, backend=backend, seed_transpiler=11, optimization_level=3)

        job = execute(QAOA_circ.circuit, backend, shots=shots, method="statevector_gpu")

        # Grab results from the job
        result = job.result()
        print("Time taken: {} sec".format(result.time_taken))

if __name__ == '__main__':
    main()