graph [
  node [
    id 0
    label "0"
    color "red"
  ]
  node [
    id 1
    label "1"
    color "green"
  ]
  node [
    id 2
    label "2"
    color "blue"
  ]
  node [
    id 3
    label "5"
    color "blue"
  ]
  node [
    id 4
    label "7"
    color "blue"
  ]
  node [
    id 5
    label "4"
    color "red"
  ]
  node [
    id 6
    label "6"
    color "red"
  ]
  node [
    id 7
    label "8"
    color "red"
  ]
  node [
    id 8
    label "3"
    color "blue"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
]
